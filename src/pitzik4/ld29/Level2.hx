package pitzik4.ld29;

class Level2 extends Level {
  public function new(scene:MainScene, ?x:Float, ?y:Float) {
    super(scene, "level2", x, y);
  }
  
  override public function begin():Void {
    super.begin();
    addText("Look at that Woof walking back and forth down there.", 11*32, 5*32);
    addText("Don't you wish that rock was walking like him?", 11*32, 5*32+8);
    addText("Well, just press C or the spacebar to go into introspection mode!", 11*32, 5*32+20, "Well, just press spacebar to go into introspection mode!", 2);
    addText("Press X near it to inspect it. Don't touch it, though,           ", 11*32, 5*32+20, "Press the down arrow near it to inspect it. Don't touch ", 1);
    addText("or you'll die!", 11*32, 5*32+28, "it, though, or you'll die!", 1);
    addText("Cycle through triggers with A and D, then press S to", 11*32, 5*32+36, "Cycle through triggers with the left and right arrow", 1);
    addText("take a command you think will be useful!", 11*32, 5*32+44, "keys, then press shift to take the command you want.", 1);
    addText("Then inspect this rock. Use S to put the command in a", 14*32, 7*32, "Then inspect this rock. Use shift to put the command", 1);
    addText("slot!", 14*32, 7*32+8, "in a slot!", 1);
    var th:Thing = Thing.makeWoof(sc, 11*32, 9*32);
    add(th);
    th = Thing.makeRock(sc, 19*32, 8*32);
    add(th);
    var door:Thing = Thing.makeDoor(sc, 29*32, 9*32);
    add(door);
  }
  
  override public function get_playerX():Float {
    return 16*32;
  }
  override public function get_playerY():Float {
    return 8*32;
  }
  public function winIt():Void {
    sc.switchLevel(new Level3(sc));
    sc.fadeIn();
  }
  override public function win():Void {
    super.win();
    sc.fadeOut(winIt);
  }
  public function loseIt():Void {
    sc.switchLevel(new Level2(sc));
    sc.fadeIn();
  }
  override public function lose():Void {
    super.lose();
    sc.fadeOut(loseIt);
  }
}
