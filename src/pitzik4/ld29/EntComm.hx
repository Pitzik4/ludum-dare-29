package pitzik4.ld29;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Graphiclist;
import com.haxepunk.graphics.Text;

class EntComm extends Entity {
  public var img:Image;
  public var nameTxt:Text;
  public var word(get, set):String;
  public function get_word():String { return nameTxt.text; }
  public function set_word(n:String):String {
    if(n == null) n = "";
    nameTxt.text = n;
    nameTxt.x = Std.int(32 - nameTxt.textWidth / 2);
    if(n == "") {
      img.alpha = 0.5;
    } else {
      img.alpha = 1.0;
    }
    return n;
  }
  public var gl:Graphiclist;
  
  public function new(?name:String) {
    super(4, 4);
    img = new Image("gfx/command.png");
    if(name == null) name = "";
    if(name == "") {
      img.alpha = 0.5;
    }
    nameTxt = new Text(name, 0, 18, 0, 0, { size: 8 });
    nameTxt.x = Std.int(32 - nameTxt.textWidth / 2);
    gl = new Graphiclist([img, nameTxt]);
    gl.scrollX = gl.scrollY = 0;
    graphic = gl;
    layer = -1;
  }
}
