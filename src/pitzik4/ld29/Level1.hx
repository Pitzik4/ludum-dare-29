package pitzik4.ld29;

class Level1 extends Level {
  public function new(scene:MainScene, ?x:Float, ?y:Float) {
    super(scene, "level1", x, y);
  }
  
  override public function begin():Void {
    super.begin();
    addText("Arrow keys to move, Z (or up) to jump!", 11*32, 7*32-16, "A and S to move, W to jump!");
    addText("If you want to use WASD, press backspace.", 11*32, 7*32, "If you want to use arrow keys, press backspace.");
    addText("If you want to use arrow keys, don't.", 11*32, 7*32+8, "If you want to use WASD, don't.");
    addText("You can toggle like this at any time.", 11*32, 7*32+16);
    addText("Try to get through this door.", 15*32, 8*32+8);
    var rib:Thing = Thing.makeRibbit(sc, 14*32, 9*32);
    rib.sprite.flipped = true;
    add(rib);
    var rock:Thing = Thing.makeRock(sc, 15*32, 9*32);
    add(rock);
    var door:Thing = Thing.makeDoor(sc, 18*32, 9*32);
    add(door);
    var woof:Thing = Thing.makeWoof(sc, 16*32, 9*32);
    add(woof);
  }
  
  override public function get_playerX():Float {
    return 384;
  }
  override public function get_playerY():Float {
    return 288;
  }
  public function winIt():Void {
    sc.switchLevel(new Level2(sc));
    sc.fadeIn();
  }
  override public function win():Void {
    super.win();
    sc.fadeOut(winIt);
  }
  public function loseIt():Void {
    sc.switchLevel(new Level1(sc));
    sc.fadeIn();
  }
  override public function lose():Void {
    super.lose();
    sc.fadeOut(loseIt);
  }
}
