package pitzik4.ld29;

class Level6 extends Level {
  public var box1:Thing;
  public var box2:Thing;
  public var box3:Thing;
  public var box4:Thing;
  public var door:Thing;
  
  public function new(scene:MainScene, ?x:Float, ?y:Float) {
    super(scene, "level6", x, y);
  }
  
  override public function begin():Void {
    super.begin();
    addText("Pro hint: Make that door move, flip the lever, and RUN.", 10*32+8, 5*32);
    addText("(Press escape or L to restart the level.)", 10*32+8, 5*32+8);
    
    var sp:Thing = Thing.makePole(sc, 11*32, 6*32);
    sp.events = ["Never"];
    sp.commands["Never"] = "Go";
    add(sp);
    
    sp = Thing.makeRock(sc, 30*32, 6*32);
    sp.events = ["Constantly"];
    sp.sprite.flipped = true;
    add(sp);
    
    sp = Thing.makePole(sc, 28*32, 6*32);
    sp.events = ["Never"];
    sp.commands["Never"] = "Go";
    add(sp);
    
    add(Thing.makeSpikes(sc, 20*32, 8*32));
    add(Thing.makeSpikes(sc, 21*32, 8*32));
    add(Thing.makeSpikes(sc, 22*32, 8*32));
    
    sp = Thing.makeLever(sc, 18*32, 11*32);
    sp.commands["Touch Killer"] = "Trigger A";
    add(sp);
    sp = Thing.makeLever(sc, 12*32, 6*32, "B");
    sp.events = ["Never"];
    add(sp);
    sp = Thing.makeLever(sc, 37*32, 9*32, "C");
    sp.commands["Touch Killer"] = "Trigger C";
    add(sp);
    
    sp = Thing.makeSeparator(sc, 9*32-10, 6*32);
    sp.setHitbox(6, 32, -10, -0);
    add(sp);
    
    box1 = Thing.makeSteel(sc, 8*32, 7*32);
    box1.acceleration.y = 0;
    add(box1);
    box2 = Thing.makeSteel(sc, 20*32, 7*32);
    box2.acceleration.y = 0;
    add(box2);
    box3 = Thing.makeSteel(sc, 21*32, 7*32);
    box3.acceleration.y = 0;
    add(box3);
    box4 = Thing.makeSteel(sc, 22*32, 7*32);
    box4.acceleration.y = 0;
    add(box4);
    
    door = Thing.makeDoor(sc, 8*32, 6*32);
    door.type = "killer";
    door.events = ["Constantly", "Wall Bump"];
    door.commands["Wall Bump"] = "Jump";
    door.maxVelocity.x = Meddler.MAX_VEL_X;
    add(door);
  }
  
  override public function get_playerX():Float {
    return 11*32;
  }
  override public function get_playerY():Float {
    return 6*32;
  }
  public function winIt():Void {
    sc.switchLevel(new Level7(sc));
    sc.fadeIn();
  }
  override public function win():Void {
    super.win();
    sc.fadeOut(winIt);
  }
  public function loseIt():Void {
    sc.switchLevel(new Level6(sc));
    sc.fadeIn();
  }
  override public function lose():Void {
    super.lose();
    sc.fadeOut(loseIt);
  }
  override public function trigger(trig:String):Void {
    super.trigger(trig);
    if(trig == "B") {
      remove(box1);
    } else if(trig == "A") {
      remove(box2); remove(box3); remove(box4);
    } else if(trig == "C") {
      add(Thing.makeSteel(sc, 32*32, 6*32));
    }
  }
}
