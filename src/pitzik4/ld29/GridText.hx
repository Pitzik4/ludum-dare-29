package pitzik4.ld29;

import com.haxepunk.HXP;
import com.haxepunk.masks.Grid;

class GridText implements ITextRenderable {
  public var grid:Grid;
  public var myOffs:Int = 0;
  public var zOffs:Float = 0;
  public static var HEX:Array<String> = ["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"];
  
  public function new(grid:Grid, yOffs:Int = 0, zOffs:Float = 0) {
    this.grid = grid;
    this.myOffs = yOffs;
    if(zOffs == -1) {
      this.zOffs = HXP.random;
    } else {
      this.zOffs = zOffs;
    }
  }
  
  public static function hash(x:Int, y:Int, zOffs:Float = 0):Int {
    return Std.int(Math.abs((Math.cos((x*2.31+y*53.21)*124.123)*412.0 + zOffs*77.18) * 16)) % 16;
  }
  public function renderText(buf:Array<Array<String>>, xOffs:Int = 0, yOffs:Int = 0, xSc:Int = 5, ySc:Int = 8):Void {
    var x:Int = Std.int(Std.int(grid.parent.x + xOffs) / xSc); var y:Int = Std.int(Std.int(grid.parent.y + yOffs) / ySc) + myOffs;
    var xOffs2:Int = x - Std.int(Std.int(grid.parent.x) / xSc); var yOffs2:Int = y - Std.int(Std.int(grid.parent.y) / ySc) - myOffs;
    x++; y--;
    for(xn in RectangleText.clamp(x,0,buf[0].length)...RectangleText.clamp(x+Std.int(grid.width/xSc),0,buf[0].length)) {
      for(yn in RectangleText.clamp(y,0,buf.length)...RectangleText.clamp(y+Std.int(grid.height/ySc),0,buf.length)) {
        if(grid.getTile(Std.int((xn - x + 2) * xSc / grid.tileWidth), Std.int((yn - y) * ySc / grid.tileHeight)))
          buf[yn][xn] = HEX[hash(xn-xOffs2, yn-yOffs2, zOffs)];
      }
    }
  }
}
