package pitzik4.ld29;

class Level5 extends Level {
  public function new(scene:MainScene, ?x:Float, ?y:Float) {
    super(scene, "level5", x, y);
  }
  
  override public function begin():Void {
    super.begin();
    addText("All aboard the Rockomotive! CHOO CHOO!", 12*32, 7*32);
    addText("\"I'm sorry, Meddler. You'll have to go on without me.\"", 24*32, 7*32);
    addText("\"Goodbye, Rockomotive. I swear I'll be back for you!\"", 25*32, 7*32+8);
    addText("(*sniff* So emotional...)", 27*32, 7*32+24);
    addText("\"I can't go through doors. It's seriously goodbye this time.\"", 48*32, 7*32);
    addText("\"Good... goobye, Rockomotive. I'll never forget you.\"", 48*32, 7*32+8);
    addText("(Meddler probably wishes I would stop putting words in his", 48*32, 7*32+24);
    addText("mouth. He's a grumpypants. If he even wears pants...)", 48*32, 7*32+32);
    var sp:Thing = Thing.makeRibbit(sc, 10*32, 8*32);
    sp.events = ["Constantly"];
    sp.commands["Constantly"] = "Go";
    add(sp);
    sp = Thing.makeRock(sc, 14*32, 8*32);
    sp.events = ["Constantly", "Wall Bump"];
    add(sp);
    add(Thing.makeSpikes(sc, 32*32, 11*32));
    add(Thing.makeSpikes(sc, 33*32, 11*32));
    add(Thing.makeSpikes(sc, 34*32, 11*32));
    sp = Thing.makeLever(sc, 36*32, 8*32);
    sp.commands["Touch Killer"] = "Trigger A";
    sp.type = "stupid";
    add(sp);
    for(i in 0...6) {
      add(Thing.makeFastWoof(sc, (15+2*i)*32, 9*32));
    }
    for(i in 0...6) {
      add(Thing.makeFastWoof(sc, (38+2*i)*32, 9*32));
    }
    sp = Thing.makePurr(sc, 30*32, 8*32);
    sp.events = ["Constantly", "Ledge"];
    sp.commands["Constantly"] = "";
    sp.commands["Ledge"] = "Jump";
    sp.commands["Wall Bump"] = "";
    add(sp);
    var door:Thing = Thing.makeDoor(sc, 52*32, 8*32);
    add(door);
  }
  
  override public function get_playerX():Float {
    return 11*32;
  }
  override public function get_playerY():Float {
    return 8*32;
  }
  public function winIt():Void {
    sc.switchLevel(new Level6(sc));
    sc.fadeIn();
  }
  override public function win():Void {
    super.win();
    sc.fadeOut(winIt);
  }
  public function loseIt():Void {
    sc.switchLevel(new Level5(sc));
    sc.fadeIn();
  }
  override public function lose():Void {
    super.lose();
    sc.fadeOut(loseIt);
  }
  override public function trigger(trig:String):Void {
    super.trigger(trig);
    for(i in 0...3) {
      var box:Thing = Thing.makeSteel(sc, (32+i)*32, 9*32);
      box.acceleration.y = 0;
      add(box);
    }
  }
}
