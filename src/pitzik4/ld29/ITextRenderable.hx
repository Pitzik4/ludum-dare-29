package pitzik4.ld29;

interface ITextRenderable {
  public function renderText(buf:Array<Array<String>>, xOffs:Int = 0, yOffs:Int = 0, xSc:Int = 5, ySc:Int = 8):Void;
}
