package pitzik4.ld29;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Graphiclist;
import com.haxepunk.graphics.Text;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Sfx;

class Inspector extends Entity {
  public var bg:Image;
  public var cover:Image;
  public var picture:Image;
  public var selector:Image;
  public var selected:Text;
  public var selectI:Int;
  public var gl:Graphiclist;
  public var inspected:IInspectable;
  public var uncover:Float;
  public var click:Sfx;
  public var comm:EntComm;
  public var pack:EntComm;
  
  public function new(inspected:IInspectable, pack:EntComm) {
    super();
    defineInputs();
    uncover = 0;
    this.pack = pack;
    click = new Sfx("sfx/click.wav");
    this.inspected = inspected;
    gl = new Graphiclist();
    gl.scrollX = gl.scrollY = 0;
    bg = new Image("gfx/inspector.png");
    gl.add(bg);
    picture = inspected.getPicture();
    picture.scale = 2;
    picture.x = 16;
    gl.add(picture);
    var title:Text = new Text(inspected.getName(), 0, 3, 0, 0, { size: 8 });
    title.x = Std.int(48 - title.textWidth / 2);
    gl.add(title);
    selector = new Image("gfx/selector.png");
    selector.x = 4; selector.y = 66;
    gl.add(selector);
    selectI = 0;
    selected = new Text(inspected.getEvents()[selectI], 0, 65, 0, 0, { size: 8 });
    selected.x = Std.int(48 - selected.textWidth / 2);
    gl.add(selected);
    comm = new EntComm(inspected.getCommands()[inspected.getEvents()[selectI]]);
    comm.gl.x = 16; comm.gl.y = 77;
    gl.add(comm.gl);
    cover = Image.createRect(88, 120, 0xFF000000);
    cover.x = 4; cover.y = 5;
    gl.add(cover);
    graphic = gl;
    setHitbox(96, 128);
    layer = -5;
  }
  public static function defineInputs():Void {
    if(Meddler.wasd) {
      Input.define("next", [Key.RIGHT]);
      Input.define("last", [Key.LEFT]);
      Input.define("swap", [Key.SHIFT]);
    } else {
      Input.define("next", [Key.A]);
      Input.define("last", [Key.D]);
      Input.define("swap", [Key.S]);
    }
  }
  public function select():Void {
    selected.text = inspected.getEvents()[selectI];
    selected.x = Std.int(48 - selected.textWidth / 2);
    comm.word = inspected.getCommands()[selected.text];
    click.play();
  }
  override public function update():Void {
    if(Input.pressed("next")) {
      selectI = (selectI+1) % inspected.getEvents().length;
      select();
    }
    if(Input.pressed("last")) {
      selectI = (selectI+inspected.getEvents().length-1) % inspected.getEvents().length;
      select();
    }
    if(Input.pressed("swap")) {
      var tmp:String = pack.word;
      pack.word = comm.word;
      comm.word = tmp;
      inspected.getCommands()[selected.text] = tmp;
      select();
    }
    x = inspected.getX() + 48 - HXP.camera.x; y = inspected.getY() - 64 - HXP.camera.y;
    x = HXP.clamp(x, 0, HXP.screen.width-width);
    y = HXP.clamp(y, 0, HXP.screen.height-height);
    if(uncover < 1) {
      uncover += 0.025;
      if(uncover >= 1) {
        gl.remove(cover);
        gl.x = 0;
        bg.x = 0;
      } else {
        if(gl.x == -1) {
          gl.x = 1;
        } else {
          gl.x = -1;
        }
        bg.x = -gl.x;
        gl.remove(cover);
        var h:Int = Std.int(120 * (1.0 - uncover));
        if(h > 0) {
          cover = Image.createRect(88, h, 0xFF000000);
          cover.x = 4;
          cover.y = 6;
          cover.y += uncover * 120;
          gl.add(cover);
        }
      }
    }
  }
}
