package pitzik4.ld29;

class Level4 extends Level {
  public var box:Thing;
  
  public function new(scene:MainScene, ?x:Float, ?y:Float) {
    super(scene, "level4", x, y);
  }
  
  override public function begin():Void {
    super.begin();
    addText("Oh, look at that lever! Levers do useful stuff, right?", 7*32, 8*32);
    addText("Press L or escape to restart the level.", 11*32, 8*32+16);
    box = Thing.makeSteel(sc, 18*32, 9*32);
    box.events = ["Never"];
    box.commands["Never"] = "";
    add(box);
    var sp:Thing = Thing.makePurr(sc, 11*32, 9*32);
    sp.sprite.flipped = true;
    sp.events = ["Constantly", "Ledge"];
    sp.commands["Constantly"] = "";
    //sp.commands["Constantly"] = "Go";
    //sp.commands["Ledge"] = "Jump";
    add(sp);
    sp = Thing.makeRibbit(sc, 14*32-6, 10*32);
    sp.events = ["Constantly", "Ledge"];
    sp.commands["Occasionally"] = "";
    sp.commands["Constantly"] = "Go";
    sp.commands["Wall Bump"] = "180";
    sp.commands["Ledge"] = "Jump";
    add(sp);
    add(Thing.makeSpikes(sc, 7*32, 12*32));
    add(Thing.makeSpikes(sc, 8*32, 12*32));
    add(Thing.makeSpikes(sc, 9*32, 12*32));
    sp = Thing.makeLever(sc, 6*32, 9*32);
    sp.commands["Touch Killer"] = "Trigger A";
    add(sp);
    var door:Thing = Thing.makeDoor(sc, 21*32, 9*32);
    add(door);
  }
  
  override public function get_playerX():Float {
    return 13*32;
  }
  override public function get_playerY():Float {
    return 9*32;
  }
  public function winIt():Void {
    sc.switchLevel(new Level5(sc));
    sc.fadeIn();
  }
  override public function win():Void {
    super.win();
    sc.fadeOut(winIt);
  }
  public function loseIt():Void {
    sc.switchLevel(new Level4(sc));
    sc.fadeIn();
  }
  override public function lose():Void {
    super.lose();
    sc.fadeOut(loseIt);
  }
  override public function trigger(trig:String):Void {
    super.trigger(trig);
    box.solidType = "eggshells";
  }
}
