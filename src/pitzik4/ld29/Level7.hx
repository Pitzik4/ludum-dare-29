package pitzik4.ld29;

class Level7 extends Level {
  public var box1:Thing;
  public var box2:Thing;
  public var box3:Thing;
  public var box4:Thing;
  public var door:Thing;
  
  public function new(scene:MainScene, ?x:Float, ?y:Float) {
    super(scene, "level7", x, y);
  }
  
  override public function begin():Void {
    super.begin();
    addText("YOU WIN!", 14*32, 7*32).size = 16;
    addText("The world is saved or something! Now you can mess around in here.", 13*32, 8*32);
    
    var sp:Thing = Thing.makePole(sc, 10*32, 9*32);
    sp.commands["Touch Meddler"] = "Launch Meddler";
    sp.commands["Never"] = "Go";
    add(sp);
    sp = Thing.makePole(sc, 21*32, 9*32);
    sp.commands["Touch Meddler"] = "Launch Meddler";
    sp.commands["Never"] = "Go";
    add(sp);
    
    for(i in 0...5) {
      sp = Thing.makeRibbit(sc, (5+i)*32, 10*32);
      sp.commands["Never"] = "Jump";
      add(sp);
    }
    
    add(Thing.makeSpikes(sc, 15*32, 5*32));
    add(Thing.makeSpikes(sc, 16*32, 5*32));
    
    sp = Thing.makeSteel(sc, 11*32, 5*32);
    add(sp);
    sp = Thing.makeSteel(sc, 20*32, 5*32);
    add(sp);
    sp = Thing.makeSteel(sc, 13*32, 5*32);
    add(sp);
    sp = Thing.makeSteel(sc, 18*32, 5*32);
    add(sp);
    
    for(i in 0...5) {
      sp = Thing.makeWoof(sc, 13*32+16+i*32, 3*32);
      sp.commands["Never"] = "Die";
      add(sp);
      sp = Thing.makePurr(sc, (22+i)*32, 10*32);
      sp.commands["Never"] = "Die";
      add(sp);
    }
  }
  
  override public function get_playerX():Float {
    return 15*32+16;
  }
  override public function get_playerY():Float {
    return 9*32;
  }
  public function winIt():Void {
    sc.switchLevel(new Level1(sc));
    sc.fadeIn();
  }
  override public function win():Void {
    super.win();
    sc.fadeOut(winIt);
  }
  public function loseIt():Void {
    sc.switchLevel(new Level7(sc));
    sc.fadeIn();
  }
  override public function lose():Void {
    super.lose();
    sc.fadeOut(loseIt);
  }
  override public function trigger(trig:String):Void {
    super.trigger(trig);
    if(trig == "B") {
      remove(box1);
    } else if(trig == "A") {
      remove(box2); remove(box3); remove(box4);
    } else if(trig == "C") {
      add(Thing.makeSteel(sc, 32*32, 6*32));
    }
  }
}
