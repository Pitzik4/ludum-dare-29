package pitzik4.ld29;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;

class Meddler extends PhysBasic implements ITextHaver {
  public static inline var ACCEL_X:Float = 0.25;
  public static inline var MAX_VEL_X:Float = 2.0;
  public static inline var MAX_VEL_Y:Float = 128.0;
  public static inline var FRICTION:Float = 0.125;
  public static inline var GRAVITY:Float = 0.25;
  public static var defSolid:Array<String> = ["solid", "solidthing"];
  public static var wasd:Bool = false;
  
  public static var editTypes:Array<String> = ["passive", "door", "killer", "solidthing"];
  
  public var sprite:Spritemap;
  public var flipped(get, set):Bool;
  public inline function get_flipped():Bool { return sprite.flipped; }
  public inline function set_flipped(val:Bool):Bool { return sprite.flipped = val; }
  public var textRect:RectangleText;
  public var owner:MainScene;
  public var dead:Bool;
  
  public function new(owner:MainScene, ?x:Int, ?y:Int) {
    super(x, y);
    this.owner = owner;
    textRect = new RectangleText(this, "<m>", "</m>");
    defineInputs();
    sprite = new Spritemap("gfx/meddler.png", 32, 32);
    #if flash
    sprite.y = 1;
    #end
    graphic = sprite;
    sprite.add("idle", [0]);
    sprite.add("run", [1,0,2,0], 6);
    sprite.add("jump", [3]);
    sprite.add("fall", [4]);
    sprite.add("die", [5]);
    sprite.play("idle");
    
    maxVelocity.x = MAX_VEL_X;
    maxVelocity.y = MAX_VEL_Y;
    friction.x = FRICTION;
    acceleration.y = GRAVITY;
    
    solidType = defSolid;
    type = "meddler";
    resetHitbox();
    layer = -2;
  }
  public function resetHitbox():Void {
    setHitbox(20, 26, -6, -6);
  }
  public static function defineAllInputs():Void {
    defineInputs();
    Inspector.defineInputs();
    MainScene.defineInputs();
    Level.activeLevel.redoWASD(wasd);
  }
  public static function defineInputs():Void {
    if(wasd) {
      Input.define("right", [Key.D]);
      Input.define("left", [Key.A]);
      Input.define("jump", [Key.W]);
      Input.define("inspect", [Key.DOWN, Key.UP]);
    } else {
      Input.define("right", [Key.RIGHT]);
      Input.define("left", [Key.LEFT]);
      Input.define("jump", [Key.UP, Key.Z]);
      Input.define("inspect", [Key.X]);
    }
  }
  
  public function updateInput():Void {
    acceleration.x = 0;
    if(Input.check("right")) {
      acceleration.x += ACCEL_X;
    }
    if(Input.check("left")) {
      acceleration.x -= ACCEL_X;
    }
    if(Input.check("jump") && collideDown) {
      jump();
    }
    if(Input.pressed("inspect")) {
      var ent:Entity = getEdit();
      owner.inspect(ent);
    }
  }
  public function jump():Void {
    velocity.y -= 5.0;
  }
  public function updateAnims():Void {
    if(collideDown) {
      if(acceleration.x != 0) {
        sprite.play("run");
        if(acceleration.x > 0) {
          flipped = false;
        } else {
          flipped = true;
        }
      } else {
        sprite.play("idle");
      }
    } else {
      if(velocity.y < 0) {
        sprite.play("jump");
      } else {
        sprite.play("fall");
      }
    }
  }
  public function getEdit():Entity {
    var coll:Entity = null;
    var w:Int = width;
    width *= 2;
    var vx:Float = sprite.flipped ? x - width/2 : x;
    for(i in 0...editTypes.length) {
      coll = collide(editTypes[i], vx, y);
      if(coll != null) break;
    }
    width = w;
    return coll;
  }
  override public function update():Void {
    if(!dead) {
      updateInput();
      updateAnims();
    }
    super.update();
  }
  public function getTextR():ITextRenderable {
    return textRect;
  }
  public function die():Void {
    owner.level.lose();
    sprite.play("die");
    dead = true;
    solidType = "encyclopedia";
    velocity.y = -5;
  }
  public function undie():Void {
    sprite.play("idle");
    dead = false;
    solidType = defSolid;
    velocity.y = 0;
  }
}
