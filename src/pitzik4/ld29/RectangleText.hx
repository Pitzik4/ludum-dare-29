package pitzik4.ld29;

import com.haxepunk.HXP;

class RectangleText implements ITextRenderable {
  public var owner:Dynamic;
  public var oTag:String;
  public var cTag:String;
  public var myOffs:Int = 0;
  public var zOffs:Float = 0;
  public static var HEX:Array<String> = ["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"];
  
  public function new(owner:Dynamic, oTag:String = "<g>", cTag:String = "</g>", yOffs:Int = 0, zOffs:Float = -1) {
    this.owner = owner;
    this.oTag = oTag;
    this.cTag = cTag;
    this.myOffs = yOffs;
    if(zOffs == -1) {
      this.zOffs = HXP.random;
    } else {
      this.zOffs = zOffs;
    }
  }
  
  public static function clamp(val:Int, min:Int, max:Int) {
    return (val < min) ? min : ((val > max) ? max : val);
  }
  public static function hash(x:Int, y:Int, zOffs:Float = 0):Int {
    return Std.int(Math.abs((Math.cos((x*2.31+y*53.21)*124.123)*412.0 + zOffs*77.18) * 16)) % 16;
  }
  public function renderText(buf:Array<Array<String>>, xOffs:Int = 0, yOffs:Int = 0, xSc:Int = 5, ySc:Int = 8):Void {
    var h:Int = Std.int(owner.height/ySc);
    var x:Int = Std.int(Std.int(owner.x + xOffs - owner.originX) / xSc); var y:Int = Std.int(Std.int(owner.y + yOffs - owner.originY) / ySc) + myOffs;
    var xOffs2:Int = x - Std.int(Std.int(owner.x - owner.originX) / xSc); var yOffs2:Int = y - Std.int(Std.int(owner.y - owner.originY) / ySc) - myOffs;
    for(xn in clamp(x,0,buf[0].length)...clamp(x+Std.int(owner.width/xSc),0,buf[0].length)) {
      for(yn in clamp(y,0,buf.length)...clamp(y+h,0,buf.length)) {
        buf[yn][xn] = oTag + HEX[hash(xn-xOffs2, yn-yOffs2, zOffs)] + cTag;
      }
    }
  }
}
