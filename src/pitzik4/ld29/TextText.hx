package pitzik4.ld29;

import com.haxepunk.graphics.Text;

class TextText implements ITextRenderable {
  public var owner:Text;
  public var oTag:String;
  public var cTag:String;
  public var myOffs:Int = 0;
  
  public function new(owner:Text, oTag:String = "", cTag:String = "", yOffs:Int = 0) {
    this.owner = owner;
    this.oTag = oTag;
    this.cTag = cTag;
    this.myOffs = yOffs;
  }
  
  public function renderText(buf:Array<Array<String>>, xOffs:Int = 0, yOffs:Int = 0, xSc:Int = 5, ySc:Int = 8):Void {
    var x:Int = Std.int(Std.int(owner.x + xOffs) / xSc); var y:Int = Std.int(Std.int(owner.y + yOffs) / ySc) + myOffs;
    if(y < 0 || y >= buf.length) return;
    var xOffs2:Int = x - Std.int(Std.int(owner.x) / xSc); var yOffs2:Int = y - Std.int(Std.int(owner.y) / ySc) - myOffs;
    for(xn in RectangleText.clamp(x,0,buf[0].length)...RectangleText.clamp(x+owner.text.length,0,buf[0].length)) {
      buf[y][xn] = oTag + owner.text.charAt(xn - x) + cTag;
    }
  }
}
