package pitzik4.ld29;

class Level3 extends Level {
  public function new(scene:MainScene, ?x:Float, ?y:Float) {
    super(scene, "level3", x, y);
  }
  
  override public function begin():Void {
    super.begin();
    addText("Having commands can be more of a hindrance than a help!", 11*32, 8*32);
    addText("You'll have to find somewhere safe to put them.", 11*32, 8*32+8);
    var sp:Thing = Thing.makePole(sc, 17*32, 13*32);
    sp.events = ["Touch Meddler"];
    add(sp);
    sp = Thing.makeSpikes(sc, 16*32-6, 10*32);
    sp.events = ["Touch Meddler"];
    add(sp);
    sp = Thing.makeSpikes(sc, 23*32, 10*32);
    sp.events = ["Touch Meddler", "Touch Meddler 2"];
    sp.commands["Touch Meddler 2"] = "Launch Meddler";
    add(sp);
    var door:Thing = Thing.makeDoor(sc, 26*32, 10*32);
    add(door);
  }
  
  override public function get_playerX():Float {
    return 12*32;
  }
  override public function get_playerY():Float {
    return 10*32;
  }
  public function winIt():Void {
    sc.switchLevel(new Level4(sc));
    sc.fadeIn();
  }
  override public function win():Void {
    super.win();
    sc.fadeOut(winIt);
  }
  public function loseIt():Void {
    sc.switchLevel(new Level3(sc));
    sc.fadeIn();
  }
  override public function lose():Void {
    super.lose();
    sc.fadeOut(loseIt);
  }
}
