package pitzik4.ld29;

import com.haxepunk.Entity;
import openfl.Assets;
import com.haxepunk.graphics.Tilemap;
import com.haxepunk.graphics.Image;
import com.haxepunk.masks.Grid;
import com.haxepunk.graphics.Text;

class Level extends Entity {
  public static var activeLevel:Level;
  
  public var grid:Grid;
  public var map:Tilemap;
  public var playerX(get, never):Float;
  public var playerY(get, never):Float;
  public var command(get, never):String;
  public function get_playerX():Float {
    return 0;
  }
  public function get_playerY():Float {
    return 0;
  }
  public function get_command():String {
    return "";
  }
  public var ents:Array<Entity>;
  public var sc:MainScene;
  public var black:Entity;
  public var blackImg:Image;
  public var wasdTexts:Array<Text>;
  public var wasdStrings:Array<String>;
  public var arrowStrings:Array<String>;
  public var wasd:Bool = false;
  public var textRs:Map<Entity, TextText>;
  
  public function new(scene:MainScene, name:String, x:Float = 0, y:Float = 0) {
    super(x, y);
    sc = scene;
    textRs = new Map<Entity, TextText>();
    ents = [];
    wasdTexts = []; wasdStrings = []; arrowStrings = [];
    var str:String = Assets.getText("lvl/" + name + ".csv");
    var width:Int = str.substring(0, str.indexOf("\n")).split(",").length;
    var height:Int = str.split("\n").length;
    map = new Tilemap("gfx/tiles.png", width*32, height*32, 32, 32);
    map.loadFromString(str);
    grid = new Grid(width*32, height*32, 32, 32);
    grid.loadFromString(str);
    this.width = width; this.height = height;
    this.graphic = map; this.mask = grid;
    this.type = "solid";
    layer = -2;
  }
  
  public function begin():Void {
    wasd = Meddler.wasd;
    activeLevel = this;
  }
  public function quit():Void {
    for(e in ents) {
      remove(e);
    }
  }
  
  public function add(e:Entity, ?t:Int):Entity {
    sc.add(e);
    ents.push(e);
    if(Std.is(e, ITextHaver)) {
      sc.addTextH(cast e);
    } else if(Std.is(e.graphic, Text)) {
      var tt:TextText = new TextText(cast(e.graphic, Text));
      sc.addTextR(tt, e, t);
      textRs[e] = tt;
    }
    return e;
  }
  public function remove(e:Entity):Entity {
    sc.remove(e);
    if(Std.is(e, ITextHaver)) {
      sc.removeTextH(cast e);
    }
    sc.removeTextR(textRs[e]);
    return e;
  }
  public function addText(text:String, x:Float, y:Float, ?wasdText:String, ?t:Int):Text {
    var txt:Text = new Text(text, x, y, 0, 0, { size: 8 });
    var ent:Entity = new Entity();
    ent.graphic = txt;
    add(ent, t);
    if(wasdText != null) {
      wasdTexts.push(txt);
      wasdStrings.push(wasdText);
      arrowStrings.push(text);
      if(Meddler.wasd) {
        txt.text = wasdText;
      }
    }
    return txt;
  }
  public function redoWASD(nwasd:Bool):Void {
    if(nwasd == wasd) return;
    if(nwasd) {
      for(i in 0...wasdTexts.length) {
        wasdTexts[i].text = wasdStrings[i];
      }
    } else {
      for(i in 0...wasdTexts.length) {
        wasdTexts[i].text = arrowStrings[i];
      }
    }
    wasd = nwasd;
  }
  public function win():Void {
    
  }
  public function lose():Void {
    
  }
  public function trigger(trig:String):Void {
    
  }
}
