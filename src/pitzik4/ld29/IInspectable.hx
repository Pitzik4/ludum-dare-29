package pitzik4.ld29;

import com.haxepunk.graphics.Image;

interface IInspectable {
  public function getPicture():Image;
  public function getName():String;
  public function getX():Float; public function getY():Float;
  public function getEvents():Array<String>;
  public function getCommands():Map<String, String>;
}
