package pitzik4.ld29;

import com.haxepunk.HXP;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.graphics.Image;
import com.haxepunk.Sfx;

class Thing extends PhysBasic implements ITextHaver implements IInspectable {
  public static inline var ACCEL_X:Float = 0.5;
  public static inline var MAX_VEL_X:Float = 1.0;
  public static inline var MAX_VEL_Y:Float = 128.0;
  public static inline var FRICTION:Float = 0.125;
  public static inline var GRAVITY:Float = 0.25;
  public static var EVENTS:Array<String> = ["Occasionally", "Constantly", "Ledge", "Wall Bump", "Touch Meddler", "Touch Killer", "Never"];
  public static var COMMANDS:Array<String> = ["Wander", "180", "Stop", "Go", "Kill Meddler", "Victory", "Die", "Launch Meddler"];
  public static var konami_cooooooode:Bool = false;
  
  public var textRect:RectangleText;
  public var walking:Bool;
  public var occasionTimer:Int;
  public var sprite:Spritemap;
  public var spriteFile:String;
  public var spriteTop:Float;
  public var thingName:String;
  public var commands:Map<String, String>;
  public var events:Array<String>;
  public var sc:MainScene;
  public var noAnims:Bool = false;
  public var dead:Bool = false;
  public var deadTime:Int = 0;
  public var actionSnd:Sfx;
  public var triggered:Bool = false;
  
  public function new(sc:MainScene, sprite:Spritemap, ?x:Int, ?y:Int, name:String = "", type:String = "passive", ?events:Array<String>) {
    super(x, y);
    textRect = new RectangleText(this);
    
    this.sc = sc;
    
    this.sprite = sprite;
    #if flash
    sprite.y += 1;
    #end
    graphic = sprite;
    sprite.play("idle");
    
    maxVelocity.x = MAX_VEL_X;
    maxVelocity.y = MAX_VEL_Y;
    friction.x = FRICTION;
    acceleration.y = GRAVITY;
    
    solidType = ["solid", "solidthing"];
    this.type = type;
    this.thingName = name;
    
    this.commands = new Map<String, String>();
    this.events = (events == null ? EVENTS : events);
    for(e in EVENTS) {
      this.commands[e] = "";
    }
    
    occasionTimer = Std.int(HXP.random * 180);
  }
  public static function makeRibbit(sc:MainScene, ?x:Int, ?y:Int):Thing {
    var sprite:Spritemap = new Spritemap("gfx/ribbit.png", 32, 32);
    sprite.add("idle", [0]);
    sprite.add("walk", [2,1,3,1], 6);
    sprite.add("die", [4]);
    var out:Thing = new Thing(sc, sprite, x, y, "Ribbit", "passive");
    out.spriteFile = "gfx/ribbit.png";
    out.spriteTop = 0;
    out.setHitbox(20, 20, -6, -12);
    out.commands["Occasionally"] = "Wander";
    out.commands["Wall Bump"] = "180";
    out.commands["Ledge"] = "180";
    out.commands["Touch Killer"] = "Die";
    return out;
  }
  public static function makeDoor(sc:MainScene, ?x:Int, ?y:Int):Thing {
    var sprite:Spritemap = new Spritemap("gfx/door.png", 32, 32);
    sprite.add("idle", [0]);
    sprite.add("walk", [0,1,2,3], 12, false);
    sprite.add("die", [0]);
    var out:Thing = new Thing(sc, sprite, x, y, "Door", "door");
    out.layer = 2;
    out.spriteFile = "gfx/door.png";
    out.spriteTop = 0;
    out.setHitbox(23, 32, -3, 0);
    out.commands["Touch Meddler"] = "Victory";
    return out;
  }
  public static function makeWoof(sc:MainScene, ?x:Int, ?y:Int):Thing {
    var sprite:Spritemap = new Spritemap("gfx/woof.png", 32, 32);
    sprite.add("idle", [0]);
    sprite.add("walk", [1,0,2,0], 6);
    sprite.add("die", [3]);
    var out:Thing = new Thing(sc, sprite, x, y, "Woof", "killer");
    out.spriteFile = "gfx/woof.png";
    out.spriteTop = 0;
    out.setHitbox(20, 22, -6, -10);
    out.commands["Constantly"] = "Go";
    out.commands["Wall Bump"] = "180";
    out.commands["Ledge"] = "180";
    out.commands["Touch Meddler"] = "Kill Meddler";
    return out;
  }
  public static function makeFastWoof(sc:MainScene, ?x:Int, ?y:Int):Thing {
    var out:Thing = makeWoof(sc, x, y);
    out.events = ["Never"];
    out.maxVelocity.x = 4.0;
    out.acceleration.x = 4.0;
    return out;
  }
  public static function makeRock(sc:MainScene, ?x:Int, ?y:Int):Thing {
    var sprite:Spritemap = new Spritemap("gfx/rock.png", 32, 32);
    sprite.add("idle", [0]);
    sprite.add("walk", [0]);
    sprite.add("die", [0]);
    var out:Thing = new Thing(sc, sprite, x, y, "Rock", "solidthing");
    out.spriteFile = "gfx/rock.png";
    out.spriteTop = 0;
    out.setHitbox(32, 32);
    return out;
  }
  public static function makeSpikes(sc:MainScene, ?x:Int, ?y:Int):Thing {
    var sprite:Spritemap = new Spritemap("gfx/spikes.png", 32, 32);
    sprite.add("idle", [0]);
    sprite.add("walk", [0]);
    sprite.add("die", [0]);
    var out:Thing = new Thing(sc, sprite, x, y, "Spikes", "killer");
    out.spriteFile = "gfx/spikes.png";
    out.spriteTop = 0;
    out.setHitbox(32, 16, 0, -16);
    out.commands["Touch Meddler"] = "Kill Meddler";
    return out;
  }
  public static function makePole(sc:MainScene, ?x:Int, ?y:Int):Thing {
    var sprite:Spritemap = new Spritemap("gfx/pole.png", 32, 32);
    sprite.add("idle", [0]);
    sprite.add("walk", [0]);
    sprite.add("die", [0]);
    var out:Thing = new Thing(sc, sprite, x, y, "Pole", "passive");
    out.spriteFile = "gfx/pole.png";
    out.spriteTop = 0;
    out.setHitbox(14, 24, -9, -8);
    return out;
  }
  public static function makePurr(sc:MainScene, ?x:Int, ?y:Int):Thing {
    var sprite:Spritemap = new Spritemap("gfx/purr.png", 32, 32);
    sprite.add("idle", [0]);
    sprite.add("walk", [1,0,2,0], 6);
    sprite.add("die", [3]);
    var out:Thing = new Thing(sc, sprite, x, y, "Purr", "killer");
    out.spriteFile = "gfx/purr.png";
    out.spriteTop = 0;
    out.setHitbox(20, 22, -6, -10);
    out.commands["Constantly"] = "Go";
    out.commands["Wall Bump"] = "180";
    out.commands["Ledge"] = "180";
    //out.commands["Touch Meddler"] = "Kill Meddler";
    out.maxVelocity.x = 4.0;
    out.acceleration.x = 4.0;
    return out;
  }
  public static function makeLever(sc:MainScene, ?x:Int, ?y:Int, tn:String = "A"):Thing {
    var sprite:Spritemap = new Spritemap("gfx/lever.png", 32, 32);
    sprite.add("idle", [0]);
    sprite.add("walk", [1]);
    sprite.add("die", [1]);
    var out:Thing = new Thing(sc, sprite, x, y, "Lever", "passive");
    out.spriteFile = "gfx/lever.png";
    out.spriteTop = 0;
    out.setHitbox(22, 19, -5, -13);
    out.commands["Touch Meddler"] = "Trigger " + tn;
    out.actionSnd = new Sfx("sfx/click.wav");
    return out;
  }
  public static function makeSteel(sc:MainScene, ?x:Int, ?y:Int):Thing {
    var sprite:Spritemap = new Spritemap("gfx/steel.png", 32, 32);
    sprite.add("idle", [0]);
    sprite.add("walk", [0]);
    sprite.add("die", [0]);
    var out:Thing = new Thing(sc, sprite, x, y, "Steel Box", "solidthing");
    out.spriteFile = "gfx/steel.png";
    out.spriteTop = 0;
    out.setHitbox(32, 32);
    return out;
  }
  public static function makeSeparator(sc:MainScene, ?x:Int, ?y:Int):Thing {
    var sprite:Spritemap = new Spritemap("gfx/separator.png", 32, 32);
    sprite.add("idle", [0]);
    sprite.add("walk", [0]);
    sprite.add("die", [0]);
    var out:Thing = new Thing(sc, sprite, x, y, "Separator", "solid");
    out.spriteFile = "gfx/separator.png";
    out.spriteTop = 0;
    out.setHitbox(1, 32, -15, -0);
    return out;
  }
  
  override public function update():Void {
    if(!dead) {
      doSomething("Constantly");
      occasionTimer--;
      if(occasionTimer <= 0) {
        occasionTimer = Std.int(HXP.random * 180);
        doSomething("Occasionally");
      }
      if(walking && atLedge()) {
        doSomething("Ledge");
      }
      if(walking && (sprite.flipped ? collideLeft : collideRight)) {
        doSomething("Wall Bump");
      }
      if(collide("meddler", x, y) != null) {
        doSomething("Touch Meddler");
      }
      if(collide("killer", x, y) != null) {
        doSomething("Touch Killer");
      }
      if(konami_cooooooode) {
        maxVelocity.x = 4.0;
        commands["Wall Bump"] = "180";
        execCommand("Go");
        execCommand("Jump");
      }
      if(!noAnims) {
        if(walking) {
          if(sprite.flipped) {
            acceleration.x = -ACCEL_X;
          } else {
            acceleration.x = ACCEL_X;
          }
          sprite.play("walk");
        } else {
          acceleration.x = 0;
          sprite.play("idle");
        }
      }
    } else {
      deadTime++;
      if(deadTime > 120) {
        sc.level.remove(this);
      }
    }
    super.update();
  }
  
  public function doSomething(occ:String):Void {
    for(s in EVENTS) {
      if(s.substr(0, occ.length) == occ) execCommand(commands[s]);
    }
    //execCommand(commands[occ]);
  }
  public function execCommand(comm:String):Void {
    if(comm == "Wander") {
      walking = !walking;
      if(walking) {
        if(HXP.random > 0.5) {
          sprite.flipped = !sprite.flipped;
        }
      }
    } else if(comm == "180") {
      sprite.flipped = !sprite.flipped;
    } else if(comm == "Stop") {
      walking = false;
    } else if(comm == "Go") {
      walking = true;
    } else if(comm == "Jump") {
      jump();
    } else if(comm == "Kill Meddler") {
      sc.player.die();
    } else if(comm == "Victory") {
      sprite.play("walk");
      noAnims = true;
      sc.level.win();
    } else if(comm == "Die") {
      sprite.play("die");
      noAnims = true;
      dead = true;
      velocity.y = -3.0;
      if(sprite.flipped) {
        velocity.x = 2.0;
      } else {
        velocity.x = -2.0;
      }
      acceleration.x = 0;
      type = "dead";
    } else if(comm == "Launch Meddler") {
      sc.player.velocity.y = -10.0;
    } else if(comm.substr(0,8) == "Trigger ") {
      if(!triggered) {
        sprite.play("walk");
        noAnims = true;
        if(actionSnd != null) {
          actionSnd.play();
        }
        sc.trigger(comm.substring(8));
      }
      triggered = true;
    }
  }
  
  public function jump():Void {
    if(collideDown)
      velocity.y -= 5.0;
  }
  
  public function atLedge():Bool {
    return collideDown && !getCollides(sprite.flipped ? -width-2 : width+2, 1);
  }
  
  public function getTextR():ITextRenderable {
    return textRect;
  }
  
  public function getPicture():Image {
    var out:Spritemap = new Spritemap(spriteFile, 32, 32);
    out.y = -spriteTop;
    return out;
  }
  public function getName():String {
    return thingName;
  }
  public function getX():Float { return x; }
  public function getY():Float { return y; }
  public function getEvents():Array<String> {
    return events;
  }
  public function getCommands():Map<String, String> {
    return commands;
  }
}
