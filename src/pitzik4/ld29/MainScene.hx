package pitzik4.ld29;

import com.haxepunk.HXP;
import com.haxepunk.Scene;
import com.haxepunk.Graphic;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Text;
import openfl.Assets;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import flash.text.Font;
import com.haxepunk.graphics.Tilemap;
import com.haxepunk.masks.Grid;
import flash.media.Sound;
import flash.media.SoundChannel;
import flash.events.Event;
import com.haxepunk.graphics.Spritemap;

class MainScene extends Scene {
  public static inline var SKY_BGC:Int = 0xFF7D9EFF;
  public static inline var MTX_BGC:Int = 0xFF000000;
  #if flash
  public static inline var INTROSPECT:String = "sfx/introspect.mp3";
  public static inline var MUSIC:String = "sfx/gameplay.mp3";
  #else
  public static inline var INTROSPECT:String = "sfx/introspect.ogg";
  public static inline var MUSIC:String = "sfx/gameplay.ogg";
  #end
  public static inline var FADE_MGN:Float = 0.033333333;
  
  public var textRenders:Array<ITextRenderable>;
  public var textMode(default,set):Bool;
  public function set_textMode(val:Bool):Bool {
    if(val != textMode) {
      if(val) {
        enableTextMode();
      } else {
        disableTextMode();
      }
    }
    return textMode = val;
  }
  public var textGraphics:Array<Text>;
  public var textEnts:Array<Entity>;
  public var player:Meddler;
  public var inspector:Inspector;
  public var command:EntComm;
  public var music:Sound;
  public var spectMusic:Sound;
  public var musicChan:SoundChannel;
  public var level:Level;
  public var black:Entity;
  public var blackImg:Image;
  public var fadeDir:Float = 0;
  public var fadeCallback:Void->Void = null;
  public var konami_code:Array<Int>;
  public var konami_code_prog:Int = 0;
  public var buttons:Spritemap;
  
  override public function begin():Void {
    konami_code = [Key.UP, Key.UP, Key.DOWN, Key.DOWN, Key.LEFT, Key.RIGHT, Key.LEFT, Key.RIGHT, Key.B, Key.A];
    defineInputs();
    textRenders = [];
    textGraphics = [];
    textEnts = [];
    music = Assets.getSound(MUSIC);
    spectMusic = Assets.getSound(INTROSPECT);
    black = new Entity();
    blackImg = Image.createRect(HXP.screen.width, HXP.screen.height, 0xFF000000);
    blackImg.scrollX = blackImg.scrollY = 0;
    blackImg.alpha = 0.0;
    black.graphic = blackImg;
    black.layer = -10;
    add(black);
    #if !flash
    var bg:Image = Image.createRect(HXP.screen.width, HXP.screen.height, MTX_BGC);
    bg.scrollX = bg.scrollY = 0;
    addGraphic(bg, 1);
    #end
    showLayer(1, false); showLayer(-1, false); showLayer(-5, false);
    HXP.screen.color = SKY_BGC;
    add(player = new Meddler(this));
    addTextH(player);
    switchLevel(new Level1(this));
    buttons = new Spritemap("gfx/buttons.png", 134, 48);
    buttons.add("arrows", [0]); buttons.add("wasd", [1]);
    buttons.play(Meddler.wasd ? "wasd" : "arrows");
    buttons.scrollX = buttons.scrollY = 0;
    var ent:Entity = new Entity(4, 188);
    ent.graphic = buttons;
    add(ent);
    ent.layer = -5;
    add(command = new EntComm());
    command.layer = -5;
    startMusic(false);
  }
  public function fadeOut(?callback:Void->Void) {
    fadeCallback = callback;
    fadeDir = FADE_MGN;
  }
  public function fadeIn(?callback:Void->Void) {
    fadeCallback = callback;
    fadeDir = -FADE_MGN;
  }
  public function musicStart(e:Dynamic):Void {
    startMusic(textMode);
  }
  public function startMusic(?textMode:Bool, ?pos:Float):Void {
    if(musicChan != null) musicChan.stop();
    var mus:Sound = textMode ? spectMusic : music;
    musicChan = mus.play(pos);
    musicChan.addEventListener(Event.SOUND_COMPLETE, musicStart);
  }
  @:access(com.haxepunk.Graphic)
  public function addTextR(ren:ITextRenderable, ?e:Entity, ?t:Int):ITextRenderable {
    if(Std.is(ren, TextText)) {
      var tt:TextText = cast(ren, TextText);
      if(t == 1) {
        e.layer = -1;
      } else if(t == 2) {
        e.layer = -2;
      } else {
        e.layer = -3;
      }
      return ren;
    }
    textRenders.push(ren);
    return ren;
  }
  public function removeTextR(ren:ITextRenderable):ITextRenderable {
    if(ren == null) return null;
    textRenders.remove(ren);
    return ren;
  }
  public function addTextH(hav:ITextHaver):ITextHaver {
    textRenders.push(hav.getTextR());
    return hav;
  }
  public function removeTextH(hav:ITextHaver):ITextHaver {
    textRenders.remove(hav.getTextR());
    return hav;
  }
  public function addTextM(map:Grid):GridText {
    var out = new GridText(map);
    textRenders.push(out);
    return out;
  }
  public function removeTextM(map:Grid):GridText {
    var out:GridText = null;
    for(t in textRenders) {
      if(Std.is(t, GridText)) {
        var gt:GridText = cast t;
        if(gt.grid == map) {
          out = gt;
          break;
        }
      }
    }
    if(out != null)
      textRenders.remove(out);
    return out;
  }
  public function switchLevel(lvl:Level):Void {
    if(textMode) textMode = false;
    if(level != null) {
      quitLevel();
    }
    player.undie();
    player.velocity.x = player.acceleration.x = 0;
    level = lvl;
    level.begin();
    add(level);
    addTextM(lvl.grid);
    player.x = lvl.playerX; player.y = lvl.playerY;
    player.sprite.flipped = false;
    HXP.camera.x = desiredCamX(); HXP.camera.y = desiredCamY();
    if(command != null)
      command.word = lvl.command;
  }
  public function quitLevel():Void {
    level.quit();
    remove(level);
    removeTextM(level.grid);
  }
  override public function render():Void {
    if(textMode) {
      var strs = renderAsText();
      for(i in 0...strs.length) {
        textGraphics[i].richText = strs[i];
      }
    }
    super.render();
  }
  public function renderAsText(xOffs:Int = 0, yOffs:Int = 0, xSc:Int = 8, ySc:Int = 8):Array<String> {
    xOffs -= Std.int(HXP.camera.x); yOffs -= Std.int(HXP.camera.y);
    var width:Int = Std.int(HXP.screen.width / xSc); var height:Int = Std.int(HXP.screen.height / ySc);
    var buf:Array<Array<String>> = [];
    for(y in 0...height) {
      buf.push([]);
      for(x in 0...width) {
        buf[y].push(" ");
      }
    }
    for(ren in textRenders) {
      ren.renderText(buf, xOffs, yOffs, xSc, ySc);
    }
    var out:Array<String> = [];
    for(i in 0...height) {
      out.push(buf[i].join(""));
    }
    return out;
  }
  public static function defineInputs():Void {
    if(Meddler.wasd) {
      Input.define("introspect", [Key.SPACE]);
      Input.define("restart", [Key.ESCAPE, Key.L]);
    } else {
      Input.define("introspect", [Key.C, Key.SPACE]);
      Input.define("restart", [Key.ESCAPE, Key.L]);
    }
    Input.define("intoggle", [Key.BACKSPACE]);
  }
  public function enableTextMode(ySc:Int = 8):Void {
    var height:Int = Std.int(HXP.screen.height / ySc);
    var none:Bool;
    if(none = textGraphics.length <= 0) {
      for(i in 0...height) {
        var text:Text = new Text("", 0, i*ySc, HXP.screen.width, ySc*2, {
          font: "font/kongtext.ttf", size: 8, wordWrap: false, resizable: false, richText: true, color: 0xFF008000
          });
        text.scrollX = text.scrollY = 0;
        text.addStyle("m", {color: 0xFFFF0000});
        text.addStyle("g", {color: 0xFF00FF00});
        text.addStyle("t", {color: 0xFFFFFFFF});
        text.smooth = false;
        textGraphics.push(text);
        textEnts.push(addGraphic(text, 1));
      }
    }
    if(!none) {
      for(t in textEnts) {
        add(t);
      }
    }
    showLayer(1);
    showLayer(-1);
    showLayer(-5);
    showLayer(0, false);
    showLayer(-2, false);
    showLayer(2, false);
    HXP.screen.color = MTX_BGC;
    startMusic(true, musicChan.position * 2.0);
  }
  public function disableTextMode():Void {
    uninspect();
    for(t in textEnts) {
      remove(t);
    }
    showLayer(1, false);
    showLayer(-1, false);
    showLayer(-5, false);
    showLayer(0);
    showLayer(-2);
    showLayer(2);
    HXP.screen.color = SKY_BGC;
    startMusic(false, musicChan.position / 2.0);
  }
  override public function update():Void {
    if(Input.pressed(Key.ANY)) {
      if(Input.pressed(konami_code[konami_code_prog])) {
        konami_code_prog++;
        if(konami_code_prog >= konami_code.length) {
          Thing.konami_cooooooode = !Thing.konami_cooooooode;
          konami_code_prog = 0;
          level.addText("IT'S THE KONAMI CODE! AAAHHHHHHHH", player.x-48, player.y-32).size = 16;
        }
      } else {
        konami_code_prog = 0;
      }
    }
    if(Input.pressed("intoggle")) {
      Meddler.wasd = !Meddler.wasd;
      Meddler.defineAllInputs();
      buttons.play(Meddler.wasd ? "wasd" : "arrows");
    }
    if(Input.pressed("introspect")) {
      textMode = !textMode;
    }
    if(Input.pressed("restart")) {
      level.lose();
    }
    if(fadeDir != 0) {
      var fadement:Float = blackImg.alpha + fadeDir;
      if(fadement <= 0 || fadement >= 1) {
        fadeDir = 0;
        if(fadement <= 0) {
          fadement = 0;
        } else {
          fadement = 1;
        }
        if(fadeCallback != null)
          fadeCallback();
        fadeCallback = null;
      }
      blackImg.alpha = fadement;
    }
    super.update();
    do {
      HXP.camera.x = HXP.lerp(HXP.camera.x, desiredCamX(), 0.05);
      HXP.camera.y = HXP.lerp(HXP.camera.y, desiredCamY(), 0.05);
    } while(player.top-HXP.camera.y < 0 || player.left-HXP.camera.x < 0 || player.bottom-HXP.camera.y > HXP.screen.height || player.right-HXP.camera.x > HXP.screen.width);
  }
  public function desiredCamX():Float {
    return player.x + 3 * player.width / 4 - HXP.screen.width / 2 + (player.flipped ? -48 : 48);
  }
  public function desiredCamY():Float {
    return player.y + 3 * player.height / 4 - HXP.screen.height / 2;
  }
  public function inspect(e:Entity):Void {
    if(uninspect()) {
      return;
    }
    if(!textMode || !Std.is(e, IInspectable)) return;
    var ins:IInspectable = cast e;
    add(inspector = new Inspector(ins, command));
  }
  public function uninspect():Bool {
    if(inspector != null) {
      remove(inspector);
      inspector = null;
      return true;
    } else {
      return false;
    }
  }
  public function trigger(trig:String):Void {
    level.trigger(trig);
  }
}
