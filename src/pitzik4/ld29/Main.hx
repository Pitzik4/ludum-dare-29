package pitzik4.ld29;

import com.haxepunk.Engine;
import com.haxepunk.HXP;

class Main extends Engine {
  public static inline var WIDTH:Int = 320;
  public static inline var HEIGHT:Int = 240;
  public static inline var STD_SCALE:Int = 2;
  
  public static var integerScaling = true;
  
  public function new() {
    super(WIDTH, HEIGHT, 60, false);
  }
  
  override public function init() {
#if debug
    HXP.console.enable();
#end
    HXP.scene = new MainScene();
  }
  
  public static function main() { new Main(); }
  
  /*override private function resize():Void {
    var width:Int = HXP.width = HXP.stage.stageWidth;
    var height:Int = HXP.height = HXP.stage.stageHeight;
    HXP.windowWidth = width;
    HXP.windowHeight = height;
    var scale:Float = Math.min(cast(width, Float) / WIDTH, cast(height, Float) / HEIGHT);
    if(integerScaling && scale > 1) {
      scale = Math.floor(scale);
    }
    HXP.screen.scale = scale;
    HXP.resize(width, height);
  }*/
}
